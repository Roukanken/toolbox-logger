const Search = require("subsequence-search");
const argparse = require("argparse");

const utils = require("./utils");

module.exports = class Opcodes {
    constructor(mod) {
        this.mod = mod;

        // Mapping opcode -> protocol
        this.nameMap = this.mod.dispatch.protocolMap.code;

        // protocol names
        this.names = Array.from(this.mod.dispatch.protocolMap.name.keys());

        this.addSearchCommand();
        this.addManipCommands();
    }

    addSearchCommand() {
        this.lastSearch = [];

        let parser = this.mod.subcommand.add("search", this.search, this);

        parser.addArgument("search_string", { narg: argparse.Const.REMAINDER });
    }

    search(args) {
        let results = Search.search(
            { rank: Search.transforms.rank(0) },
            this.names,
            args.search_string
        ).map(res => res.input);

        this.mod.command.message("Top results:");

        for (let i = 0; i < Math.min(5, results.length); i++) {
            this.mod.command.message(`   ${i}: ${results[i]}`);
        }

        this.lastSearch = results;
    }

    addManipCommands() {
        let parser1 = this.mod.subcommand.add("black", args => {
            let name = this.resolve(args.name);
            if (name) this.blacklist(name);
        });

        let parser2 = this.mod.subcommand.add("white", args => {
            let name = this.resolve(args.name);
            if (name) this.whitelist(name);
        });

        let parser3 = this.mod.subcommand.add("remove", args => {
            let name = this.resolve(args.name);
            if (name) this.remove(name);
        });

        parser1.addArgument("name", {});
        parser2.addArgument("name", {});
        parser3.addArgument("name", {});
    }

    blackCommand(args) {}

    resolve(name) {
        if (name.match(/^@[0-9]+$/)) {
            let index = parseInt(name.substring(1));
            if (index > this.lastSearch.length)
                this.mod.command.message("Outside of results!");
            else return this.lastSearch[index];
        } else return name;
    }

    remove(name, list) {
        if (list == undefined || list === "whitelist")
            this.mod.settings.whitelist = this.mod.settings.whitelist.filter(
                x => x != name
            );

        if (list == undefined || list === "blacklist")
            this.mod.settings.blacklist = this.mod.settings.blacklist.filter(
                x => x != name
            );
    }

    whitelist(name) {
        let alreadyIn = this.mod.settings.whitelist.find(x => x == name);
        if (alreadyIn)
            this.mod.command.message(`Opcode ${name} is already whitelisted!`);
        else {
            this.mod.settings.whitelist.push(name);
            this.remove(name, "blacklist");

            this.mod.command.message(`Added!`);
        }
    }

    blacklist(name) {
        let alreadyIn = this.mod.settings.blacklist.find(x => x == name);
        if (alreadyIn)
            this.mod.command.message(`Opcode ${name} is already blacklisted!`);
        else {
            this.mod.settings.blacklist.push(name);
            this.remove(name, "whitelist");

            this.mod.command.message(`Added!`);
        }
    }

    getName(opcode, fromServer) {
        let name = this.nameMap.get(opcode);
        if (name) return name;

        return (fromServer ? "S_" : "C_") + opcode;
    }

    getOptions(name) {
        let allowed = this.mod.settings.mode == utils.LoggingMode.BLACKLIST;

        let inWhite = this.mod.settings.whitelist.find(x => x == name);
        let inBlack = this.mod.settings.blacklist.find(x => x == name);

        if (inWhite) allowed = true;
        if (inBlack) allowed = false;

        return { allowed };
    }
};
