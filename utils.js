module.exports.LoggingMode = {
    WHITELIST: "whitelist",
    BLACKLIST: "blacklist",
};

module.exports.pad = (str, length) => {
    str = "" + str;
    const len = Math.max(length - str.length, 0);

    return str + " ".repeat(len);
};

module.exports.trim = (str, length) => {
    if (!length) return str;

    str = "" + str;
    let result = str.substring(0, length - 4);

    if (result.length < str.length) result = result + "....";

    return result;
};
