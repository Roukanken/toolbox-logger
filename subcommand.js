const ArgumentParser = require("argparse").ArgumentParser;

module.exports = class Subcommand {
    constructor(mod, name, data) {
        this.mod = mod;
        this.mainParser = new ArgumentParser(data);
        this.subParsers = this.mainParser.addSubparsers();

        mod.command.add(name, this.execute, this);
        this.functions = {};
    }

    add(name, func, ref) {
        if (ref) func = func.bind(ref);

        this.functions[name] = func;

        let subparser = this.subParsers.addParser(name, { addHelp: true });

        subparser.addArgument("--function-name", {
            action: "storeConst",
            constant: name,
            defaultValue: name,
            required: false,
        });

        return subparser;
    }

    execute(...args) {
        try {
            let parsed = this.mainParser.parseArgs(args);

            return this.functions[parsed.function_name](parsed);
        } catch (error) {
            this.mod.command.message(error);
            console.log("error:", error);
            return;
        }
    }
};

// Nasty hack, because it was exiting the proxy on invalid input
ArgumentParser.prototype.exit = function(status, message) {
    if (message) {
        if (status === 0) {
            this._printMessage(message);
        } else {
            this._printMessage(message, process.stderr);
        }
    }

    throw new Error(message);
};
