const SettingsUI = require("tera-mod-ui").Settings;

module.exports = class UI {
    constructor(mod) {
        this.mod = mod;

        this.init();
        this.addCommand();
    }

    addCommand() {
        let parser = this.mod.subcommand.add("settings", this.toggle, this);
    }

    toggle() {
        if (this.ui) this.ui.show();
        else this.mod.command.message("Ui not available!");
    }

    init() {
        // Copied from Caali's instant everything
        this.ui = null;
        if (global.TeraProxy.GUIMode) {
            this.ui = new SettingsUI(
                this.mod,
                require("./structure"),
                this.mod.settings,
                { height: 232 }
            );
            this.ui.on("update", settings => {
                this.mod.settings = settings;
            });

            this.destructor = () => {
                if (this.ui) {
                    this.ui.close();
                    this.ui = null;
                }
            };
        }
    }
};
