const { LoggingMode } = require("../utils");

module.exports = [
    {
        key: "mode",
        name: "Default logging mode",
        type: "select",
        options: Object.entries(LoggingMode).map(([name, key]) => {
            return { name, key };
        }),
    },
    {
        key: "data_length",
        name: "Max lenght of data",
        type: "number",
    },
];
