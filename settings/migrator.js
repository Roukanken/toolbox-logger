const { LoggingMode } = require("../utils");

const DefaultSettings = {
    mode: "whitelist",
    data_length: null,
    whitelist: [],
    blacklist: [],
};

module.exports = function MigrateSettings(from_ver, to_ver, settings) {
    return Object.assign({}, DefaultSettings, settings);
};
