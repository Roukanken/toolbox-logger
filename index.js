const SettingsUi = require("./settings");
const utils = require("./utils");
const Subcommand = require("./subcommand");
const Opcodes = require("./opcodes");

module.exports = class Logger {
    constructor(mod) {
        this.mod = mod;

        mod.subcommand = new Subcommand(mod, ["log", "logger"]);
        this.settingsUi = new SettingsUi(mod);
        this.opcodes = new Opcodes(mod);

        this.mod.hook("*", "raw", this.loggerHook.bind(this));
    }

    loggerHook(code, data, fromServer, fake) {
        let name = this.opcodes.getName(code, fromServer);
        let options = this.opcodes.getOptions(name);

        if (!options.allowed) return;

        let len = data.length;
        const hex_data = utils.trim(
            data.toString("hex"),
            this.mod.settings.data_length * 2
        );

        console.log(
            `${utils.pad(name, 50)} | ${utils.pad(
                data.length,
                5
            )} | ${hex_data}`
        );
    }
};
